Rails.application.routes.draw do
	get 'help', to: 'pages#help'
	get 'about', to: 'pages#about'
	get 'contact', to: 'pages#contact'
	get 'estimate', to: 'pages#estimate'
	get 'packages', to: 'pages#packages'
	root to: "pages#index"
end
